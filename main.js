// Instance the tour
var tour = new Tour({
    steps: [
        {
            element: "myNavbar",
            title: "Welcome!",
            content: "Welcome to FungAssist - I'm your guideline-assistant! Do you want to learn more?"
        },
        {
            element: "questionnaire-wrapper",
            title: "How can I help?",
            content: "I know all about particular diseases as I was fed with the most recent guidelines about each topic."
        },
        {
            element: "questionnaire-wrapper",
            title: "How do I work?",
            content: "You can feed me with information about your patient and while you're answering my questionnaire I'm searching for ways of further diagnosis, therapy and more information that will interest you."
        },
        {
            element: "questionnaire-choices tooltip-frontend intro",
            title: "Let's start!",
            content: "First things first: Choose the disease you want to learn more about."
        },
        {
            element: "questionnaire",
            title: "Filling out my questionnaire",
            content: "While you're filling out the questionnaire I'm continuously showing you the excerpts that are relevant for your patient."
        },
        {
            element: "sidenav",
            title: "Your personal excerpts",
            content: "Here I will display your personal excerpts from relevant guidelines. The more questions you answer the more excerpts I can find."
        },
        {
            element: "text-center explain-anchor-color",
            title: "Different types...",
            content: "I discriminate 3 different types of guideline-excerpts. Check out the symbols to find out what they mean!"
        },
        {
            element: "anchor-popup-nav text-right",
            title: "",
            content: "Check out the symbols to the right. I can tell you why I'm showing you this quote, you can highlight it for later or just close it."
        },
        {
            element: "anchor-popup-text table-responsive alert alert-info",
            title: "Click to find out more!",
            content: "If you click on the quote I will show you the full excerpt. Click on underlined words to check their definition. Source tells you where this excerpt is coming from."
        },
        {
            element: "fs-nav-dots fs-show",
            title: "Check your progress",
            content: "Here you can see that you're making progress answering my questionnaire. Depending on your answer new questions will pop up to specify the information you're giving."
        },
        {
            element: "limit-search-box",
            title: "Narrow your search!",
            content: "You can filter your search by choosing specific guidelines if you don't want all guidelines to be searched."
        },
        {
            element: "fs-form-wrap fill",
            title: "Publish your case!",
            content: "You can print out a summary of your case or publish it to discuss it with the community."
        },
        {
            element: "nav-cases",
            title: "See all the cases!",
            content: "Click here to see all the open cases and start your commitment!"
        },
    ]});

// Initialize the tour
tour.init();

// Start the tour
tour.start();